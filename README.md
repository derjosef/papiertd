# PapierTD
In this paper styled tower defence game, placed towers are obstacles
that the enemy must avoid.

This is just a showcase right now. To make this game fun,
it will need more levels, more towers and enemies,
and a few extras like unlocking mechanisms,
customizable difficulties and the ability to save.
To try out the game, download Godot and import this repository.

Currently, programming is done using GDScript only.
If you have found a bug then please report that under issues.
Also, feel free to create merge requests.

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

### Fonts
The Lemon Tuesday font used in this project is licensed under the SIL Open Font License, Version 1.1.
This license is available at: <http://scripts.sil.org/OFL>.
The font itself can be found here: <https://www.dafont.com/lemon-tuesday.font>.

### Godot
This project uses Godot Engine, released under the permissive MIT license.
This license is available at: <https://godotengine.org/license>.
Check out the source code here: <https://github.com/godotengine/godot>.
Copyright (c) 2007-2020 Juan Linietsky, Ariel Manzur.
Copyright (c) 2014-2020 Godot Engine contributors.

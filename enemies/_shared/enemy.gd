extends Area2D
class_name Enemy


export(float) var full_speed = 100.0
export(int) var reward = 4

const MASS = 5.0
const ARRIVE_DISTANCE = 16.0

var path = []

enum {SPAWNING, ACTIVE, ESCAPING, ZOMBIE}
var status = SPAWNING

var spawning_finished = false
onready var speed = full_speed
var full_health: float
var health: float
#var destination = Vector2()
var velocity = Vector2()
var offset = Vector2((randf()-.5) * 20, (randf()-.5) * 20)

# signals get defined at creation in enemies.gd
signal enemy_escaped
#signal path_arrived

#const mask = preload("res://enemies/_shared/enemy_lightmask.tres")
func _ready():
	#set_material(mask)
	set_collision_layer_bit(0, false)


func _process(delta):
	move_to(path[0] + offset, delta)
	if position.distance_to(path[0] + offset) < ARRIVE_DISTANCE:
		path.remove(0)
#		if not spawning_finished:
#			spawning_finished = true
#			set_collision_layer_bit(0, true)
#		# check if called multiple times.. case with status?
#		if len(path) == 0:
#			# at end of path and active
#			set_process(false)
#			call_deferred("queue_free")
#			return
#		if len(path) == 1 and get_collision_layer_bit(0):
#			# at end of map and active
#			escape()
		match status:
			SPAWNING:
				set_collision_layer_bit(0, true)
				status = ACTIVE
			ACTIVE:
				if len(path) == 1:
					escape()
					status = ESCAPING
			ESCAPING:
				if len(path) == 0:
					set_process(false)
					call_deferred("queue_free")
					status = ZOMBIE


func move_to(world_position, delta):
	var desired_velocity = (world_position - position).normalized() * speed
	var steering = desired_velocity - velocity
	velocity += steering / MASS
	position += velocity * delta
	rotation = velocity.angle()


func hit(damage: float):
	health -= damage
	if health <= 0 and get_collision_layer_bit(0):
		die()
	else:
		$base.set_modulate(Color(1, float(health)/full_health, float(health)/full_health))
		speed = (float(health)/full_health + 1 )/2 * full_speed


func blink():
	$status.play("blink")


func die():
	set_process(false)
	set_collision_layer_bit(0, false)
	$shape.set_deferred("disabled", true)
	#call_deferred("$shape.disabled = true")
	$animation.play("die")
	yield($animation, "animation_finished")
	global.get_level().money += reward
	queue_free()


func escape():
	set_collision_layer_bit(0, false) # towers don't shoot any more
	emit_signal("enemy_escaped")
	#$animation.play("escape")
	#yield($animation, "animation_finished")
	#call_deferred("queue_free")

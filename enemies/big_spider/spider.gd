extends Enemy

func die():
	call_deferred("spawn_spiders")
	.die()

func spawn_spiders():
	var enemy_controller = global.get_level().enemy_controller
	enemy_controller.spawn(0, 10, 1, position)

func escape():
	emit_signal("enemy_escaped", 10)
	.escape()

extends Node

var target = 1

onready var level = global.get_level()


func update_paths(destination_index = 1):
	for enemy in get_children():
		# check if enemy even needs a path
		if len(enemy.path) == 1:
			continue
		enemy.path = level.navi.get_nav_path(enemy.path[0], destination_index)


func is_enemy_blocked(destination_index = 1):
	var left_to_right = level.navi.astar.get_point_path(0,1)
	var top_to_bottom = true #navi.astar.get_point_path(2,3)
	var enemies_can_leave = true
	if not left_to_right or not top_to_bottom:
		return false
	
	for enemy in get_children():
		#TODO only precalculate and save for later use?
		if len(enemy.path) == 1:
			continue
		if not level.navi.get_nav_path(enemy.path[0], destination_index):
			level.alert('you cant cage an enemy ' + str(enemy.name) + ":" +  str(len(enemy.path)))
			level.alert(str(enemy.path))
			enemy.blink()
			enemies_can_leave = false
	
	return enemies_can_leave

func spawn(type: int, quantity: int, strength: float, position: Vector2 = Vector2.ZERO):
	print([type, quantity, strength])
	for _i in range(quantity):
		var enemy_instance = level.enemies[type].instance()
		enemy_instance.position = position if position else \
				Vector2(-10, 300) - Vector2(randi() % 30, randi() % 200 -100)
		enemy_instance.full_health = strength
		enemy_instance.health = strength
		enemy_instance.path = level.navi.get_nav_path(enemy_instance.position, target)
		enemy_instance.connect("enemy_escaped", level, "_on_enemy_escaped")
		add_child(enemy_instance)

#sent from waves
func _on_send_wave(type: int, quantity: int, strength: float):
	spawn(type, quantity, strength)

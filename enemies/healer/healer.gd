extends Enemy


func _on_timer_timeout():
	heal()
	$animation.play("heal")
	# use setget?
	hit(0)


func heal():
	health = min(full_health, health + full_health * .2)

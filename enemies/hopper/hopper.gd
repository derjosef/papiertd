extends Enemy

const JUMP_DISTANCE = 130


func _process(_delta):
	# parent _process is called automaticly
	# use super() with godot 4?
	if status != ACTIVE:
		return
	for i in len(path) - 6:
		if position.distance_to(path[-i-1] + offset) < JUMP_DISTANCE:
			path = path.slice(len(path)-i-2, len(path) -1)
			jump_to_pos(path[0])
			break


func jump_to_pos(destination: Vector2):
	set_process(false)
	set_collision_layer_bit(0, false)
	look_at(destination)
	var origin = position
	# set position so further changes start from here
	#$base.position = origin
	position = destination
	$base.global_position = origin
	z_index += 5
	var speed: float = 1.0
	$tween.interpolate_property($shape, "global_position", 
			origin,
			destination,
			speed,
			Tween.TRANS_LINEAR, Tween.EASE_IN)
	$tween.interpolate_property($base, "global_position", 
			origin,
			destination,
			speed,
			Tween.TRANS_LINEAR, Tween.EASE_IN)
	$tween.interpolate_property($base, "scale", 
			Vector2.ONE,
			Vector2.ONE * 1.5,
			speed/2,
			Tween.TRANS_QUAD, Tween.EASE_OUT)
	$tween.interpolate_property($base, "scale", 
			Vector2.ONE * 1.5,
			Vector2.ONE,
			speed/2,
			Tween.TRANS_QUAD, Tween.EASE_IN, speed/2)

	# animate moving to position
	$tween.start()
	yield($tween, "tween_all_completed")
	z_index -= 5
	set_collision_layer_bit(0, true)
	set_process(true)


extends VBoxContainer


var active_tower: Tower = null
onready var money: int = global.get_level().money
#export (PackedScene) var buy_upgrade_button

func _on_tower_button(tower_node: Tower):
	clear_active_tower()
	
	active_tower = tower_node
	active_tower.radius_visible(true)
	
	get_parent().visible = true
	$h_box_container/TextureRect.texture = \
		tower_node.get_node("turret").texture
	var current_priority = active_tower.targeting
	print(current_priority)
	$priority_container.get_node(current_priority).modulate = Color(.5,1,1,1.2)
	#print("pressed: " + tower_node.name.trim_prefix("@").split("@", true)[0])
	update_upgrade_buttons()


func updated_money(new_money) -> void:
	if not active_tower:
		return
	money = new_money
	update_upgrade_buttons()

func update_upgrade_buttons():
	$buy_upgrade0_button.text = active_tower.upgrades[0]["display_text"]
	$buy_upgrade0_button/cost.text = "¤ " + str(active_tower.upgrades[0]["cost"])
	$buy_upgrade0_button.set_disabled(not active_tower.can_upgrade(0))
	
	$buy_upgrade1_button.text = active_tower.upgrades[1]["display_text"]
	$buy_upgrade1_button/cost.text = "¤ " + str(active_tower.upgrades[1]["cost"])
	$buy_upgrade1_button.set_disabled(not active_tower.can_upgrade(1))
	
	$sell_button/cost.text = "¤ " + str(active_tower.cost * .5)


func _unhandled_input(event: InputEvent):
	if event is InputEventMouseButton and event.is_pressed():
		clear_active_tower()
		get_tree().set_input_as_handled()


func clear_active_tower():
	get_parent().visible = false
	if active_tower:
		active_tower.radius_visible(false)
		active_tower = null


func _on_sell_button():
	global.get_level().tower_controller.remove(active_tower)
	clear_active_tower()


func _on_priority_button(priority: String) -> void:
	active_tower.change_target_method(priority)


func connect_tower_button(tower_instance):
	tower_instance.connect("button_pressed", self, "_on_tower_button")


func _on_buy_upgrade_button_pressed(upgrade_number):
	active_tower.upgrade(upgrade_number)

extends GridContainer


onready var level = global.get_level()

export (PackedScene) var tower_button
var placing_tower
var active_buy_button_instance
var dragging: bool = true
var prices: Array = []

# gets called at init
func add_tower_buttons(towers: Array) -> void:
	for tower_reference in towers:
		var tower = tower_reference.instance()
		# var button_instance = $_buy_button.duplicate()
		var button_instance = tower_button.instance()
		button_instance.connect("button_down", self, "enable_pointer", [tower_reference, button_instance])
		button_instance.connect("pressed", self, "_on_button_pressed")
		button_instance.connect("button_up", self, "_on_button_up")
		button_instance.get_node("base/symbol").texture = \
			tower.get_node("turret").texture
		button_instance.get_node("name").text = tower.name
		# * in metafores is to small - use ¤
		button_instance.get_node("cost").text = "¤" + var2str(tower.cost)
		prices.append(tower.cost)
		add_child(button_instance)
		tower.free()


func updated_money(new_money) -> void:
	# iterate through buy_tower_buttons and set disabled if to expensive
	for i in range(prices.size()):
		var tower: Button = get_child(i + 2)
		if not tower.disabled and prices[i] > new_money:
			tower.disabled = true
			tower.modulate = Color(1,1,1,.5)
			continue
		if tower.disabled and prices[i] <= new_money:
			tower.disabled = false
			tower.modulate = Color(1,1,1,1)


func _process(_delta) -> void:
	# moves pointer
	if placing_tower:
		var point = $pointer.get_global_mouse_position()
		var tower_center = level.navi.get_tower_center(point)
		if tower_center:
			point = tower_center
		$pointer.global_position = point
		#var can_place = level.enemy_controller.


func _input(event) -> void:
	if not placing_tower or dragging:
		return
	if event.is_action_pressed("click"):
		yield(get_tree().create_timer(.1), "timeout")
		if placing_tower:
			$pointer.visible = true
	
	if event.is_action_released("click"):
		add_tower()


# enables pointer to place tower when called
# called by tower buy buttons
func enable_pointer(tower_reference, button_instance) -> void:
	#print(tower_reference._bundled["variants"][3])
	placing_tower = tower_reference
	active_buy_button_instance = button_instance
	active_buy_button_instance.modulate = Color(.5,1,1,1.2)
	$pointer.texture = button_instance.get_node("base/symbol").texture

	dragging = true
	yield(get_tree().create_timer(.1), "timeout")
	if dragging:
		$pointer.visible = true


# clicked and released button -> next click can place tower
func _on_button_pressed() -> void:
	dragging = false
	$pointer.visible = false
	# enable buttons - no multi-touch - button press overrides

# emmited when stopped clicking, everywhere not just on the button
func _on_button_up() -> void:
	if dragging and placing_tower:
		add_tower()


func add_tower() -> void:
	$pointer.visible = false
	# change color back
	active_buy_button_instance.modulate = Color(1,1,1,1)
	
	var mouse = get_global_mouse_position()
	var position = level.navi.get_tower_center(mouse)
	if position:
		level.tower_controller.add(position, placing_tower)
	else:
		level.alert("not placeable")
	placing_tower = false

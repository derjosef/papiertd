extends Button

func set_disabled(disabled):
	.set_disabled(disabled)
	
	var color = $cost.modulate
	if disabled:
		color.a = .5
		$cost.text = "bought!"
	else:
		color.a = 1
	$cost.modulate = color

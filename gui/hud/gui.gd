extends Control

export var upcoming_wave_time = 4

signal next_wave
signal pause

var cutoff = 0
func _process(delta : float) -> void:
	#moves the nextwaves icons
	add_offset(delta / upcoming_wave_time)


#called from game
func update_health_counter(value: int) -> void:
	$health_counter.text = str(value)

func update_wave_counter(value: int) -> void:
	$wave_counter.text = str(value)

func update_money_counter(value: int) -> void:
	$money_counter.text = str(value)
	$buy_tower_button_container.updated_money(value)
	$texture_rect/active_tower_container.updated_money(value)


func add_offset(offset: float) -> void:
	for wave in $Path2D.get_children():
		wave.unit_offset += offset
		if wave.get_unit_offset() >= 1:
			wave.queue_free()


#called from waves
func add_wave(enemy_name) -> void:
	#create path follow and create signal for changes in time
	var node = PathFollow2D.new()
	var child = Sprite.new()
	$Path2D.add_child(node)
	node.loop = false
	node.add_child(child)
	var sprite = 'res://enemies/' + enemy_name + '/base.png'
	child.texture = load(sprite)


func _on_pause_button() -> void:
	print('pressed pause button')
	emit_signal('pause')
	#$'..'._on_pause()


func _on_next_wave_button() -> void:
	print('pressed next wave button')
	emit_signal('next_wave')


func _on_waves_jump_ahead(time: float) -> void:
	add_offset(time / upcoming_wave_time)


func _on_quit_button_pressed():
	global.load_titlescreen()

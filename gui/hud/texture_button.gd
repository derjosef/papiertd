extends TextureButton

# class_name MyTextureButton
# load with
# var button: MyTextureButton = MyTextureButton.new()
onready var color := get_modulate()

func _ready():
	#visible = false
	#rect_min_size = Vector2(0,50)
	_on_TextureButton_focus_exited()

func set_textures(texturepath_normal: String,
				  texturepath_disabled: String = "") -> void:
	texture_normal = load(texturepath_normal)
	if texturepath_disabled:
		texture_disabled = load(texturepath_disabled)
	
	if not get_signal_connection_list("pressed"):
		set_disabled(true)
	visible = true

func set_disabled(disable: bool):
	if disable:
		_on_TextureButton_focus_exited()
	.set_disabled(disable)

func _on_TextureButton_focus_entered():
	if not disabled:
		color.a = .95
		set_modulate(color)

func _on_TextureButton_focus_exited():
	if not disabled:
		color.a = .70
		set_modulate(color)

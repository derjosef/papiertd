extends TextureRect

func _ready():
	pass # Replace with function body.

func _on_resume_button_down():
	$resume_mark.visible = true


func _on_resume_button_up():
	$resume_mark.visible = false


func _on_quit_button_down():
	$quit_mark.visible = true


func _on_quit_button_up():
	$quit_mark.visible = false


func _on_resume_pressed():
	global.get_level().pause()


func _on_quit_pressed():
	global.load_titlescreen()

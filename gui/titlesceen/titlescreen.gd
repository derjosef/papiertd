extends CenterContainer

var off_position
var off_rotation
func _ready():
	#$start.set_textures("res://assets/out2.png")
	#$TextureButton2.set_textures("res://assets/out2.png")
	randomize()
	off_position = Vector2.ONE.rotated(randi()) * (randi() % 80)
	rect_position = off_position
	off_rotation = (randi() % 50 - 25) / 3


var cutoff = 0
const graph_speed = 1.6
const cutoff_graph = 1/graph_speed
func _process(delta):
	if cutoff < cutoff_graph:
		$VBoxContainer/title.material.set_shader_param("cutoff", cutoff * graph_speed)
	if cutoff > .9:
		return
	var zoom = cutoff * .5 + .5
	rect_scale = Vector2.ONE * zoom
	rect_position = off_position * (1 - cutoff)
	rect_rotation = off_rotation * (1 - cutoff)
	cutoff += delta / 3
	#$desk.rect_scale = Vector2.ONE * .75


func _on_start_pressed():
	global.load_level("res://levels/level1.tscn")
	#global.load_level("res://levels/shared/game.tscn")


func _on_about_pressed():
	global.load_titlescreen();


func _on_quit_pressed():
	get_tree().quit()

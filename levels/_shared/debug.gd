extends Node2D

onready var navi = get_node("../navi")

const BASE_LINE_WIDTH = 1
const DRAW_COLOR = Color('#fff')

func _draw():
	for point_index in navi.astar.get_points():
		if navi.astar.is_point_disabled(point_index):
			continue
		var point = navi.astar.get_point_position(point_index)
		point = navi.cell_to_world(Vector2(point.x, point.y))
		for connection in navi.astar.get_point_connections(point_index):
			connection = navi.astar.get_point_position(connection)
			connection = navi.cell_to_world(Vector2(connection.x, connection.y))
			#draw_line(point, connection, DRAW_COLOR, BASE_LINE_WIDTH, true)
		draw_circle(point, BASE_LINE_WIDTH*3, DRAW_COLOR)


func _on_Timer_timeout():
	update()

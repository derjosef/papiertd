extends Node2D
class_name Level

export var lives = 10
export var money = 500 setget set_money

# for the use in other nodes
onready var navi: Node = $navi
onready var tower_controller: Node = $tower_controller
onready var enemy_controller: Node = $enemy_controller
var towers: Array
var enemies: Array


func _ready():
	$gui.update_health_counter(lives)
	$gui/buy_tower_button_container.add_tower_buttons(towers)
	$gui.update_money_counter(money)

func _unhandled_input(event):
	if event.is_action_pressed("ui_end"):
		pause()


func alert(notification: String):
	print(notification)


func _on_enemy_escaped(damage: int = 1):
	lives -= damage
	$gui.update_health_counter(lives)
	if lives == 0:
		print('you loose')
		return

func set_money(new_money):
	money = new_money
	if is_inside_tree():
		$gui.update_money_counter(new_money)


func pause():
	get_tree().paused = !get_tree().paused
	if get_tree().paused:
		$pause_popup.show()
	else:
		$pause_popup.hide()

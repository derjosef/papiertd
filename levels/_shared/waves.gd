extends Node

signal send_wave(type, quantity, strength)
signal jump_ahead(time)
signal update_wave_counter(number)

export var rng_seed: int = 364

var wave_counter: int = 0
var _rng = RandomNumberGenerator.new()


func _ready():
	gui_update()
	_rng.set_seed(rng_seed)


#called from next wave timer and gui
func next_wave():
	wave_counter += 1
	var wave = _get_next_wave()
	print('next wave ' +  str(wave_counter))
	emit_signal('send_wave', wave[0], wave[1], wave[2])
	gui_update()
	$next_wave_timer.start()
	#$next_wave.set_wait_time(duration)


func gui_update():
	emit_signal('update_wave_counter', wave_counter)
	emit_signal('jump_ahead', $next_wave_timer.time_left)
	#print($next_wave_timer.time_left)


func _get_next_wave():
	var type: int
	var quantity: int
	var strength: int

	if wave_counter == 1:
		type = 0
		quantity = 1
		strength = 2
	elif wave_counter < 5:
		type = 0
		quantity = _rng.randi() % 2 + 1 + wave_counter / 3
		strength = 1
	else:
		type = _rng.randi() % 2
		if type == 1:
			quantity = 1 + wave_counter / 30
		else:
			quantity = _rng.randi() % 3 + 1 + wave_counter / 15
		strength = _rng.randi() % 3 + wave_counter / 10 + 1
	# send boss
	if not wave_counter % 5:
		type = 1
		strength = wave_counter / 10 + 1
		quantity = 1

	return [type, quantity, strength]

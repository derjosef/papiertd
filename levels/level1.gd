extends Level

func _ready():
	towers = [
		preload("res://towers/cannon/cannon.tscn"),
		preload("res://towers/laser/laser.tscn"),
		#preload("res://towers/electro/electro.tscn"),
		preload("res://towers/detonator/detonator.tscn"),
	]
	enemies = [
		preload("res://enemies/standard/standard.tscn"),
		preload("res://enemies/hopper/hopper.tscn"),
		preload("res://enemies/healer/healer.tscn"),
		#preload("res://enemies/big_spider/spider.tscn"),
	]
	._ready()

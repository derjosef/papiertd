class_name Tower
extends Node2D

var rotation_speed = 10
var areas_in_range = []
var targeting : String = "get_first"
var get_target = funcref(self, targeting)
var last_target

onready var level := global.get_level()

# warning-ignore:unused_class_variable
export var damage: int = 1
export var cost: int = 100
export var radius: float = 50 #setget set_radius
onready var upgrades := $upgrades.get_children()


func _ready():
	level.money -= cost
	
	# randomize base and turret sprites
	$base.rotation_degrees = [0,90,180,270][randi() % 4] - 2 + randi() % 4
	$base.flip_v = randi() % 2
	$base.flip_h = $base.flip_v
	$turret.flip_v = randi() % 2
	
	# override "helper" object
	var shape = CircleShape2D.new()
	$range/circle.set_shape(shape)
	set_radius(radius)


func change_target_method(target_method):
	get_target = funcref(self, target_method)
	targeting = target_method


func set_radius(new_radius):
	radius = new_radius
	# set hidden texture to right size
	$range/visual.rotation_degrees = randi()
	$range/visual.scale = Vector2.ONE * radius * 0.0016
	# set radius of collision shape
	#if not $range/circle.get_shape():
	#	return
	$range/circle.get_shape().set_radius(radius)


func radius_visible(visibility: bool):
	$range/visual.visible = visibility


func _process(delta):
	if areas_in_range.empty():
		return
	last_target = get_target.call_func()
	var angle = $turret.get_angle_to(last_target.position)
	angle = sign(angle) * min(abs(angle), rotation_speed * delta)
	$turret.rotate(angle)
	# -0.0001 to compensate for loose precision
	if abs(angle) > rotation_speed * delta - 0.0001:
		return
	if $cooldown.is_stopped():
		$cooldown.start()
		shoot()


func shoot():
	pass


# get enemy with shortest path to go
func get_first():
	var path_length = INF
	var first
	if areas_in_range.has(last_target):
		path_length = len(last_target.path)
		first = last_target
	for enemy in areas_in_range:
		if path_length <= len(enemy.path):
			continue
		path_length = len(enemy.path)
		first = enemy
	return first
#	return areas_in_range[0]


func get_last():
	var path_length = 0
	var last
	if areas_in_range.has(last_target):
		path_length = len(last_target.path)
		last = last_target
	for enemy in areas_in_range:
		if path_length >= len(enemy.path):
			continue
		path_length = len(enemy.path)
		last = enemy
	return last


func get_close():
	var distance = INF
	var closest
	for enemy in areas_in_range:
		if distance <= position.distance_to(enemy.position):
			continue
		distance = position.distance_to(enemy.position)
		closest = enemy
	return closest


func get_weak():
	var lowest_health = INF
	var weakest
	for enemy in areas_in_range:
		if lowest_health <= enemy.health:
			continue
		lowest_health = enemy.health
		weakest = enemy
	return weakest


func get_strong():
	var highest_health = -INF
	var strongest
	for enemy in areas_in_range:
		if highest_health >= enemy.health:
			continue
		highest_health = enemy.health
		strongest = enemy
	return strongest


func get_same():
	# might return null if target removed
	# wont be called blindly
	return last_target


func _on_area_entered(area):
	areas_in_range.append(area)


func _on_area_exited(area):
	areas_in_range.erase(area)


func can_upgrade(id: int) -> bool:
	if upgrades[id]["bought"]:
		return false
	if upgrades[id]["cost"] > level.money:
		return false
	return true


func upgrade(id: int):
	if not can_upgrade(id):
		return

	upgrades[id]["bought"] = true

	if not upgrades[id]["function"]:
		print("no upgrade found")
		return
	funcref(self, upgrades[id]["function"]).call_func()

	level.money -= upgrades[id]["cost"]


extends Area2D

var damage: int
var direction
var speed = 150
var distance = 500

#func init(starting_pos, direction, damage_init):
#	position = starting_pos
#	damage = damage_init
func _ready():
	$tween.interpolate_property(self, "position", position,Vector2(distance, 0).rotated(direction) + position,
			distance/speed, Tween.TRANS_QUAD, Tween.EASE_OUT)
	$tween.interpolate_property(self, "rotation", 0, 360 * 10,
			10, Tween.TRANS_LINEAR, 0)
	$tween.start()


func _on_tween_completed(_object, _key):
	queue_free()


var collided = 0
func _on_bullet_area_entered(area):
	collided += 1
	if collided > 1:
		return
	area.hit(damage)
	queue_free()

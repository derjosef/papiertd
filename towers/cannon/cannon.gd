extends Tower

export (PackedScene) var bullet

var size = 1
var additional_speed = 0

func shoot():
	var bullet_instance = bullet.instance()
	bullet_instance.position = $turret/muzzle.get_global_position()
	bullet_instance.direction = $turret.rotation
	bullet_instance.damage = damage
	bullet_instance.speed += additional_speed
	bullet_instance.scale = Vector2.ONE * size
	$bullets.add_child(bullet_instance)


func upgrade_range():
	set_radius(radius * 1.5)
	additional_speed = 30


func upgrade_damage():
	damage *= 2
	size = 1.5

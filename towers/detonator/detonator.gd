extends Tower

export (PackedScene) var explosive

#func _ready():
#	shoot = funcref(self, shoot_once)

func dispatch():
	# shoot.call_func()
	var explosive_instance = explosive.instance()
	explosive_instance.position = $turret/muzzle.get_global_position()
	explosive_instance.destination = last_target.position \
				+ Vector2(60,0).rotated(last_target.rotation)
	explosive_instance.damage = damage
	$explosives.add_child(explosive_instance)

func shoot():
	dispatch()
	if $upgrades/upgrade2.bought:
		yield(get_tree().create_timer(.5), "timeout")
		if areas_in_range.empty():
			return
		dispatch()
		yield(get_tree().create_timer(.5), "timeout")
		if areas_in_range.empty():
			return
		dispatch()

func upgrade_range():
	set_radius(150)


func upgrade_damage():
	pass
	#damage *= 2

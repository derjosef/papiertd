extends Area2D

var damage: int
var destination: Vector2
var speed := .5
var radius: float = 20


#func init(starting_pos, direction, damage_init):
#	position = starting_pos
#	damage = damage_init
func _ready():
	# approximate flying duration and angular point
	speed = cos(position.distance_squared_to(destination)/pow(200,2)*.7) *.8
	print(position.distance_to(destination), str(speed))
	var hight = cos(position.distance_squared_to(destination)/pow(200,2)) /2
	$tween.interpolate_property(self, "position", 
			position, 
			destination,
			speed,
			Tween.TRANS_LINEAR, Tween.EASE_IN)
	$tween.interpolate_property($sprite, "scale", 
			Vector2(.1,.1),
			Vector2(hight,hight),
			speed/2,
			Tween.TRANS_QUAD, Tween.EASE_OUT)
	$tween.interpolate_property($sprite, "scale", 
			Vector2(hight,hight),
			Vector2(.15,.15),
			speed/2,
			Tween.TRANS_QUAD, Tween.EASE_IN, speed/2)
	$tween.interpolate_property(self, "rotation",
			0,
			(randi() % 6 * 2 - 5),
			speed,
			Tween.TRANS_LINEAR, Tween.EASE_IN)
	$tween.start()
	#yield($tween, "tween_completed")


func _on_tween_all_completed():
	# explode
	for area in get_overlapping_areas():
		area.hit(damage)
	queue_free()

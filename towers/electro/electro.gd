extends Tower

export var beam_color: Color
var target
var beam_show = false
var width


func shoot():
	target = get_target.call_func()
	get_target = funcref(self, "get_same")
	beam_show = true
	$beam_duration.start()

func _on_area_exited(area):
	._on_area_exited(area)
	if not areas_in_range or \
	not areas_in_range.has(target):
		beam_show = false
		get_target = funcref(self, "get_last")


func _on_beam_duration_timeout():
	if beam_show:
		beam_show = false
		get_target = funcref(self, "get_last")
		target.hit(damage)

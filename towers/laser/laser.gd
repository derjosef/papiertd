extends Tower

# red = 5 -> wont be strongly effected by parent modulate
var beam_color = Color(5,.1,.1,.9)
var target
var beam_show = false
var width


func _process(delta):
	update()
	if beam_show:
		var end = target.get_position()
		$turret/end.global_position = end
		target.hit(damage * delta)


func _draw():
	if beam_show:
		var end = target.get_global_position() - position
		var origin = $turret/muzzle.get_global_position() - position
		draw_line(origin, end, beam_color, width, true)
		width = width - .1

func shoot():
	target = get_target.call_func()
	get_target = funcref(self, "get_same")
	width = 6
	beam_show = true
	$turret/end/particles_2d.emitting = true
	$beam_duration.start()

func _on_area_exited(area):
	._on_area_exited(area)
	if not areas_in_range or \
	not areas_in_range.has(target):
		beam_show = false
		$turret/end/particles_2d.emitting = false
		get_target = funcref(self, targeting)


func _on_beam_duration_timeout():
	if beam_show:
		beam_show = false
		$turret/end/particles_2d.emitting = false
		get_target = funcref(self, targeting)

func upgrade_damage():
	damage *= 2

func upgrade_cooldown():
	$cooldown.wait_time *= .8
	$beam_duration.wait_time *= 1.1

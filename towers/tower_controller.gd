extends Node2D


onready var level := global.get_level()
export var gui_path: NodePath
onready var gui = get_node(gui_path)


func add(position: Vector2, tower: PackedScene) -> bool:
	#create the tower
	var tower_instance = tower.instance()
	if tower_instance.cost > level.money:
		tower_instance.free()
		level.alert('not enough money...')
		return false

	# check if the space is available
	var cells = level.navi.get_tower_cells(position)
	if level.navi.is_occupied(cells):
			tower_instance.free()
			level.alert('space occupied...')
			return false

	level.navi.remove_cells(cells)
	# check if enemies are boxed in
	if not level.enemy_controller.is_enemy_blocked(1):
		level.navi.add_cells(cells)
		tower_instance.free()
		level.alert('no path for enemies...')
		return false

	tower_instance.get_node("button").connect(
		"pressed", gui, "_on_tower_button", [tower_instance])
	tower_instance.position = position
	add_child(tower_instance)
	
	# update enemies
	level.enemy_controller.update_paths()
	return true


func remove(tower: Tower):
	var position = tower.position
	level.money += tower.cost * .5
	tower.queue_free()
	
	var cells = level.navi.get_surrounding_cells(position, 2)
	level.navi.add_cells(cells)
	level.enemy_controller.update_paths()

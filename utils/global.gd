extends Node

onready var root = get_tree().get_root()
onready var current_scene = root.get_child(root.get_child_count() - 1)

func load_level(path):
	get_tree().set_quit_on_go_back(false)
	#ProjectSettings.set_setting("display/window/size/height", 300)
	call_deferred("_deferred_goto_scene", path)

func _notification(what):
	# back key enables pause first, if paused exit game
	if what == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST:
		if current_scene.get_name().begins_with("level"):
			if not get_tree().paused:
				current_scene.pause()
				return
		get_tree().quit()

func load_titlescreen():
	get_tree().paused = false
	get_tree().set_quit_on_go_back(true)
	call_deferred("_deferred_goto_scene", ProjectSettings.get_setting("application/run/main_scene"))

func get_level() -> Level:
	assert(current_scene.get_name().begins_with("level"))
	return current_scene
	
func _deferred_goto_scene(path):
	# safe to remove current scene now
	current_scene.free()
	
	var scene = load(path)
	current_scene = scene.instance()
	root.add_child(current_scene)
	
	# make it compatible with the SceneTree.change_scene() API.
	get_tree().set_current_scene(current_scene)

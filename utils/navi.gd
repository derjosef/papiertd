extends Node

export(Vector2) var map_size = Vector2(30, 18)
export(Vector2) var offset = Vector2(0, 0)
export(int) var cell_size = 32
var max_index = map_size.x * map_size.y

onready var astar = AStar.new()
var half_cell = Vector2(cell_size, cell_size) / 2

#enum {bottom, left, top, right}

const pivot_width = 2

# point work with astar node
# cell work with local x and y position

# ↓y →x
# 4,5,6
# 7,8,9

func _ready():
	map_all_cells()
	add_left_right()

func add_left_right():
	#left
	var start_index = 4 + ceil((map_size.y - pivot_width) / 2) * map_size.x
	#var start_index = 4 + 5 * map_size.x
	#print(map_size.y / 2)
	var end_index = 4 + floor((map_size.y + pivot_width -1 ) / 2) * map_size.x + 1
	#var end_index = 4 + 6 * map_size.x + 1
	if not astar.has_point(0):
		astar.add_point(0, Vector3(-1, (map_size.y -1) / 2, 0))
	for i in range(start_index, end_index, map_size.x):
		if not astar.are_points_connected(0,i):
			astar.connect_points(0, i)

	#right
	start_index = start_index + map_size.x - 1
	end_index = end_index + map_size.x - 1
	if not astar.has_point(1):
		astar.add_point(1, Vector3(map_size.x + 10, (map_size.y-1) /2, 0))
	for i in range(start_index, end_index, map_size.x):
		if not astar.are_points_connected(1,i):
			astar.connect_points(1, i)


func add_top_bottom(): #2,3
	#top
	var start_index = 4 + ceil((map_size.x - pivot_width) / 2)
	var end_index = 4 + floor((map_size.x + pivot_width) / 2) + 1
	# or floor to always be even or odd like the mapsize 
	astar.add_point(2, Vector3(map_size.x /2, -1, 0))
	for i in range(start_index, end_index):
		astar.connect_points(2, i)
	
	#bottom
	start_index = ceil((map_size.x - pivot_width) / 2) - 4
	end_index = floor((map_size.x + pivot_width) / 2) + 1 - 4
	astar.add_point(3, Vector3(map_size.x /2, map_size.y + 1, 0))
	for  i in range(max_index - end_index + 1, max_index - start_index + 1):
		astar.connect_points(3, i)
	

func map_all_cells():
	for y in range(map_size.y):
		for x in range(map_size.x):
			var point_position = Vector3(x, y, 0)
			var point_index = calculate_point_index(point_position)
			astar.add_point(point_index, point_position)
			# For every cell in the map, we add the left and the top
			# because these cells are already created and will be enough to make every connection
			if x != 0:
				astar.connect_points(point_index, point_index - 1)
			if y != 0:
				astar.connect_points(point_index, point_index - map_size.x)


func add_cells(cell_array):
	for cell in cell_array:
		astar.set_point_disabled(calculate_point_index(cell), false)


func remove_cells(cell_array):
	for cell in cell_array:
		astar.set_point_disabled(calculate_point_index(cell))
	return true


func calculate_point_index(cell):
	if cell.x < 0: return 0
	if cell.y < 0: return 1
	if cell.x >= map_size.x: return 2
	if cell.y >= map_size.y: return 3
	return 4 + cell.x + map_size.x * cell.y


func cell_to_world(cell):
	return cell * cell_size + half_cell + offset


func world_to_cell(position_world: Vector2):
	position_world -= offset
	return Vector2(int(position_world.x) / cell_size, int(position_world.y) / cell_size)

func get_nav_path(start_world, end_index):
	var start_index = calculate_point_index(world_to_cell(start_world))
	#var start_index = 0
	#var end_index = calculate_point_index(world_to_cell(end_world))
	#var end_index = 1
	var path = astar.get_point_path(start_index, end_index)
	var path_world = []
	for point in path:
		var point_world = cell_to_world(Vector2(point.x, point.y))
		path_world.append(point_world)
	#print(calculate_point_index(path[-1]))
	return path_world


func get_tower_cells(position_world):
	var cell_array = PoolVector2Array()
	var upper_left_cell = world_to_cell(position_world - half_cell)
	for y in range(2):
		for x in range(2):
			cell_array.append(upper_left_cell + Vector2(x,y))
	print(' ' + str(cell_array))
	return cell_array

func get_surrounding_cells(position_world, size = 2):
	var first_cell
	if size % 2: #odd
		first_cell = world_to_cell(position_world)
	else: #even
		first_cell = world_to_cell(position_world - half_cell)
	#size = float(size)

	var cell_array = PoolVector2Array()
	for y in range(-size/2,size/2):
		for x in range(-size/2,size/2):
			cell_array.append(first_cell - Vector2(x,y))
	return cell_array


func is_occupied(cell_array: PoolVector2Array):
	for cell in cell_array:
		if cell.x < 0 or cell.y < 0 or cell.x >= map_size.x or cell.y >= map_size.y:
			return true
		var point_index = 4 + cell.x + map_size.x * cell.y
		if astar.is_point_disabled(point_index):
			return true
	return false


func get_tower_center(position_world):
	var rel_position = position_world - half_cell - offset
	if not rel_position == rel_position.abs():
		return false
	var upper_left_cell = world_to_cell(position_world - half_cell)
	if upper_left_cell.x > map_size.x - 2 or upper_left_cell.y > map_size.y - 2:
		return false
	# add half cell to midpoint of upper left cell
	return cell_to_world(upper_left_cell) + half_cell
